//
// Created by tnt11 on 10.12.2023.
//
#include "bmp_processing.h"
#include "processing_codes.h"
#include "processing_image.h"

#include <stdio.h>

#define PIXEL_WEIGHT 3
#define ALIGN 4

#define BF_TYPE 0x4D42
#define BF_REVERSED 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_COMPRESSION 0
#define BI_X_PELS_PER_METER 2834
#define BI_Y_PELS_PER_METER 2834
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0


uint8_t calculate_padding(uint64_t width) {
    uint8_t row = width * PIXEL_WEIGHT;
    return (ALIGN - (row % ALIGN)) % ALIGN;
}
struct bmp_header createHeader(struct image const *source) {
    uint64_t w = source->width;
    uint64_t h = source->height;
    uint8_t padding = calculate_padding(w);

    uint64_t size_image = w * h * PIXEL_WEIGHT;
    const uint64_t size_file = sizeof(struct bmp_header) + size_image + (h * padding);

    struct bmp_header head =
            {
            .bfType = BF_TYPE,
            .bfileSize = size_file,
            .bfReserved = BF_REVERSED,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BI_SIZE,
            .biWidth = w,
            .biHeight = h,
            .biPlanes = BI_PLANES,
            .biBitCount = BI_BIT_COUNT,
            .biCompression = BI_COMPRESSION,
            .biSizeImage = size_image,
            .biXPelsPerMeter = BI_X_PELS_PER_METER,
            .biYPelsPerMeter = BI_Y_PELS_PER_METER,
            .biClrUsed = BI_CLR_USED,
            .biClrImportant = BI_CLR_IMPORTANT
            };
    return head;
}
enum BMP_VALIDATOR_STATUS bmp_validator(struct bmp_header header){
    if(header.bfType != BF_TYPE){
        fprintf(stderr,"invalid in header: BF_TYPE");
        return BMP_HEADER_TYPE_INVALID;
    }if(header.bfReserved != BF_REVERSED){
        fprintf(stderr,"invalid in header: BF_RESERVED ");
        return BF_REVERSED_INVALID;
    }if(header.biPlanes != BI_PLANES){
        fprintf(stderr,"invalid in header: BI_PLANES");
        return BI_PLANES_INVALID;
    }if(header.biBitCount != BI_BIT_COUNT){
        fprintf(stderr,"invalid in header: BI_BIT_COUNT");
        return BI_BIT_COUNT_INVALID;
    }if(header.biCompression != BI_COMPRESSION){
        fprintf(stderr,"invalid in header: BI_COMPRESSION");
        return BI_COMPRESSION_INVALID;
    }
    if(header.biClrUsed != BI_CLR_USED){
        fprintf(stderr,"invalid in header: BI_CLR_USED");
        return BI_CLR_USED_INVALID;
    }if(header.biClrImportant != BI_CLR_IMPORTANT){
        fprintf(stderr,"invalid in header: BI_CLR_USED");
        return BI_CLR_IMPORTANT_INVALID;
    }
    return BMP_HEADER_IS_VALID;
}
enum FROM_BMP_READING_STATUS from_bmp(FILE* input, struct image* img){
    struct bmp_header header = {0};
    size_t count = fread(&header, sizeof(struct bmp_header), 1, input);
    if(count != 1){
        fprintf(stderr, "invalid bmp file");
        return INVALID_BMP_HEADER;
    }
    if(bmp_validator(header)){
        fprintf(stderr,"Invalid header");
        return INVALID_HEADER_PARAMETERS;
    }
    enum CREATING_IMAGE_STATUS status = create_image(header.biWidth,header.biHeight, img);
    if(status != CREATED_SUCCESSFULLY){
        fprintf(stderr,"can't create a file");
        return ERROR_WHILE_CREATING_IMAGE_FROM_BMP;
    }
    for (uint64_t row = 0; row < img->height; row++) {

        count = fread(img->data + row * img->width, sizeof(struct pixel), img->width, input);

        if (count != img->width) {
            return ERROR_WHILE_CREATING_IMAGE_FROM_BMP;
        }
        if (fseek(input,calculate_padding(img->width),SEEK_CUR)) return CANNOT_READ_HEADER;

    }
    fclose(input);
    return BMP_IMAGE_READ_SUCCESSFULLY;
}
enum TO_BMP_WRITING_STATUS to_bmp(FILE* output, struct image* output_image){

    struct bmp_header header = createHeader(output_image);
    size_t cnt = fwrite(&header, sizeof (struct bmp_header), 1,output);
    if(cnt != 1){
        fprintf(stderr,"Can't write header of image");
        return CANNOT_WRITE_HEADER;
    }
    for(uint64_t i = 0; i < output_image->height;i++){
        if(fwrite(output_image->data + i * output_image->width, sizeof(struct pixel), output_image->width, output) != output_image->width){
            fprintf(stderr,"Error while writing header");
            return CANNOT_WRITE_HEADER;
        }
        if (fseek(output,calculate_padding(output_image->width),SEEK_CUR)) return CANNOT_WRITE_HEADER;
    }
    return WRITTEN_SUCCESSFULLY;
}



