//
// Created by tnt11 on 10.12.2023.
//
#include "image_validator.h"
#include "processing_image.h"

#include <inttypes.h>
#include <malloc.h>

enum CREATING_IMAGE_STATUS create_image(uint64_t width,uint64_t height,struct image* image){
    if (width == 0 || height == 0) {
        return INVALID_IMAGE_DIMENSIONS;
    }

    uint64_t amount_pixels = width * height;
    struct pixel* data = malloc(amount_pixels * sizeof(struct pixel));

    if(data == NULL){
        return CANNOT_ALLOCATE_MEMORY;
    }
    image->width = width;
    image->height = height;
    image->data = data;
    if(!image_characteristic_validator(*image)) {
        return INVALID_CHARACTERISTIC;
    }
    return CREATED_SUCCESSFULLY;
}
void delete_image(struct image* toDel){
    free(toDel->data);
    toDel->data = NULL;
}
enum TRANSFORMING_STATUS rotate_neg_90(struct image* old_image, struct image* transformed_image){
    uint64_t width = old_image->width;
    uint64_t height = old_image->height;
    if(create_image(height,width,transformed_image) != CREATED_SUCCESSFULLY){
        return ERROR_WHILE_CREATING_IMAGE;
    }
    if(old_image->data == NULL){
        return INPUT_IMAGE_HAVE_NULL_PIXELS;
    }
    for (size_t i = 0; i < height; i++) {
        for (size_t j = 0; j < width; j++) {
            transformed_image->data[height * j + (height - i - 1)] = old_image->data[i * width + j];
        }
    }

    delete_image(old_image);
    return TRANSFORMED_SUCCESSFULLY;
}
enum TRANSFORMING_STATUS rotate(struct image* img,struct image* new_image,uint32_t angle){
//    struct image new_img;
    if(angle == 90 ||  angle == -270) {
        struct image tmp_image;
        if(rotate_neg_90(img,&tmp_image) == TRANSFORMED_SUCCESSFULLY){
            struct image tmp_image2;
            if(rotate_neg_90(&tmp_image,&tmp_image2) == TRANSFORMED_SUCCESSFULLY){
                return rotate_neg_90(&tmp_image2,new_image);
            }
        }
    }
    if(angle == 180 || angle == -180){
        struct image tmp_img;
            if(rotate_neg_90(img,&tmp_img) == TRANSFORMED_SUCCESSFULLY){
                return rotate_neg_90(&tmp_img,new_image);
            }
    }
    if(angle == 270 || angle == -90)
    {return rotate_neg_90(img,new_image);
    }
    if(angle == 0){
        *new_image = *img;
        return TRANSFORMED_SUCCESSFULLY;
    }
    return FAILED_TO_ROTATE;
}
