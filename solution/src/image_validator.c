#include "image_validator.h"

#include <stdbool.h>

#define COUNT_VALID_ANGLES 7
//
// Created by tnt11 on 10.12.2023.
//
bool is_angle_valid(uint64_t angle){
    uint64_t valid_angels[] = {-270,-180,-90, 0, 90, 180, 270};
    uint8_t size_of_valid_array = COUNT_VALID_ANGLES;
    for(uint8_t i = 0; i < size_of_valid_array; i++){
        if(valid_angels[i] == angle){
            return true;
        }
    }
    return false;
}
bool image_characteristic_validator(struct image image){
    if(image.width <= 0)
        return false;
    if(image.height <= 0)
        return false;
    return true;
}

