//
// Created by tnt11 on 15.12.2023.
//
#include "IOprocessing.h"
#include "bmp_processing.h"
#include "image_validator.h"
#include "processing_image.h"


enum input_read_status_to_img read_file(char* filename,FILE** readFile){
    *readFile = fopen(filename,"rb");
    if(!(*readFile)){
        perror("can't open the file");
        return FILE_N0T_FOUND;
    }
    return FILE_READ_SUCCESSFULLY;
}

enum OUTPUT_WRITER_STATUS write_file(char* filename,FILE** output){
    *output = fopen(filename,"wb");
    if(!*output){
        fprintf(stderr,"can't open output file");
    }
    return OUTPUT_FILE_READ_SUCCESSFULLY;
}
