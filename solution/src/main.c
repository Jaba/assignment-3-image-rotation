#include "image_validator.h"
#include "IOprocessing.h"
#include "bmp_processing.h"
#include "processing_image.h"

#include <stdio.h>
#include <stdlib.h>


int main( int argc, char** argv ) {
    if (argc != 4) {
        fprintf(stderr, "Wrong arguments");
        return -1;
    }
    char* end;
    long val = strtol(argv[3], &end, 10);
    if (!is_angle_valid((int)val)) {
        fprintf(stderr, "Wrong angle value!");
        return -1;
    }
    struct image img = {0};
    FILE *input = NULL;
    read_file(argv[1],&input);
    from_bmp(input,&img);
    struct image new_img;
    FILE* output = NULL;
    if(rotate(&img,&new_img,(int64_t) val) == TRANSFORMED_SUCCESSFULLY) {
        write_file(argv[2], &output);
        to_bmp(output,&new_img);
        delete_image(&new_img);
    }else{
        delete_image(&new_img);
        delete_image(&img);
        fprintf(stderr,"Error while transforming images");
        return -1;
    }
    return 0;
}
