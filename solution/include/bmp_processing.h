//
// Created by tnt11 on 10.12.2023.
// BMP file checked in validator and will be returned there to deserializing
//

#include "image.h"
#include "processing_codes.h"
#include <stdio.h>

#ifndef IMAGE_TRANSFORMER_BMP_DECIRIALIZE_H

#define IMAGE_TRANSFORMER_BMP_DECIRIALIZE_H

struct bmp_header createHeader(struct image const *source);
uint8_t calculate_padding(uint64_t width);
enum BMP_VALIDATOR_STATUS bmp_validator(struct bmp_header header);
enum FROM_BMP_READING_STATUS from_bmp(FILE* inputFile,  struct image* img);
enum TO_BMP_WRITING_STATUS to_bmp(FILE* output, struct image* output_image);


#endif //IMAGE_TRANSFORMER_BMP_DECIRIALIZE_H
