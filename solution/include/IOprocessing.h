//
// Created by tnt11 on 15.12.2023.
//

#include "image.h"
#include "processing_codes.h"

#include <stdio.h>
#ifndef IMAGE_TRANSFORMER_IOPROCESSING_H
#define IMAGE_TRANSFORMER_IOPROCESSING_H

enum input_read_status_to_img read_file(char* filename,FILE** readFile) ;

enum OUTPUT_WRITER_STATUS write_file(char* filename,FILE** output);

#endif //IMAGE_TRANSFORMER_IOPROCESSING_H
