//
// Created by tnt11 on 10.12.2023.
//



#ifndef IMAGE_TRANSFORMER_PROCESSING_IMAGE_H
#define IMAGE_TRANSFORMER_PROCESSING_IMAGE_H
#include "image.h"
#include "processing_codes.h"

#include <stdbool.h>
#include <stdint.h>


enum CREATING_IMAGE_STATUS create_image(uint64_t width, uint64_t height,struct image* image);

enum TRANSFORMING_STATUS rotate_neg_90(struct image* old_image, struct image* transformed_image);

enum TRANSFORMING_STATUS rotate(struct image* img,struct image* new_image,uint32_t angle);

void delete_image(struct image* toDel);
#endif
