//
// Created by tnt11 on 10.12.2023.
//
#include "image.h"
#include "processing_codes.h"

#include <stdbool.h>
#include <stdio.h>

#ifndef IMAGE_TRANSFORMER_IMAGE_VALIDATOR_H
#define IMAGE_TRANSFORMER_IMAGE_VALIDATOR_H


bool image_characteristic_validator(struct image image);

bool is_angle_valid(uint64_t angle);


#endif //IMAGE_TRANSFORMER_IMAGE_VALIDATOR_H
